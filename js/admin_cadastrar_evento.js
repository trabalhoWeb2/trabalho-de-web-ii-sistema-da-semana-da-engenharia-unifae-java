var input_names;
var quantChar;

/*-- Função que controla o aparecimento da caixa de alerta ao usuario: esta deve receber um elemento, 
 o seu estado (se esta correto o preenchimento do campo ou não), 
 e um alert que indica qual caixa deverá mudar de estado (alert ou modal_alert) --*/
function alert_personalizado(campo, estado, alert) {
    if (!estado) {
        $("#" + alert).text("Preencha o(s) campo(s) " + campo + " corretamente").css({"text-align": "center", "font-weight": "bold"});
        $("#" + alert).css({"display": "block"});
    } else {
        $("#" + alert).css({"display": "none"});
    }
}

function valida_inputs(input_names, alert) {
    var id;
    var string = "";
    var aux = true;

    $(input_names).each(function (index, el) {
        id = el.id;
        if ($("#" + id).val() == "") {
            $("#" + id).css({"border": "2px solid red"});
            string += " " + el.name;
            aux = false;
        } else {
            $("#" + id).css({"border": "2px solid green"});
        }
    });

    alert_personalizado(string, aux, alert);

    return aux;
}

jQuery(document).ready(function ($) {

    $("#horaEvento").change(function () {
        valida_inputs($("#horaEvento"));
    })

    /*-- Função para validar a data do evento --*/
    $("#dataEvento").focusout(function () {
        var dataCadastroEvento = ($('#dataEvento').val()); //Pega a data que o usuario informou
        var dataCadastro = dataCadastroEvento.split("-"); //Converte a data do evento para o formato AAAA-MM-DD do tipo String
        /*-- Pega a data do sistema --*/
        var dia = moment().date() //Pega o dia atual
        var mes = moment().month() + 1 //Pega o mes atual e soma 1 (o mes vai de 0 a 11)
        var ano = moment().year(); //Pega o ano atual

        /*-- Pega a data do evento informada pelo usuario e atribui cada valor em uma variavel --*/
        var diaCadastro = dataCadastro[2] //pega o valor na posição 2 da string e atribui para o dia do evento
        var mesCadastro = dataCadastro[1] //pega o valor na posição 1 da string a atribui para o mes do evento
        var anoCadastro = dataCadastro[0] //pega o valor na posição 1 da string a atribui para o ano do evento

        /*-- Comparações para validar a data do evento --*/
        if (ano > anoCadastro) {
            $("#alert").text("Data Inválida, informe outra data por favor").css({"text-align": "center", "font-weight": "bold"});
            $("#alert").css({"display": "block"});
            $("#dataEvento").val("").css({"border": "2px solid red"});
            $("#dataEvento").focus();
        } else {
            if (ano < anoCadastro) {
                valida_inputs($("#dataEvento"));
                $("#alert").css({"display": "none"});
            } else {
                if (mes > mesCadastro && ano < anoCadastro) {
                    valida_inputs($("#dataEvento"));
                    $("#alert").css({"display": "none"});
                } else {
                    if (mes <= mesCadastro && dia < diaCadastro) {
                        valida_inputs($("#dataEvento"));
                        $("#alert").css({"display": "none"});
                    } else {
                        if (mes < mesCadastro) {
                            valida_inputs($("#dataEvento"));
                            $("#alert").css({"display": "none"});
                        } else {
                            $("#alert").text("Data Inválida, informe outra data por favor").css({"text-align": "center", "font-weight": "bold"});
                            $("#alert").css({"display": "block"});
                            $("#dataEvento").val("").css({"border": "2px solid red"});
                            $("#dataEvento").focus();
                        }
                    }
                }
            }
        }
    });

    /*-- Validando as informações de Cadastro para não haverem campos vazios --*/
    $("#cadastrar").click(function () {
        input_names = new Array();
        quantChar = $("#descricao").val().length;

        $(".form-control, .form-control-file").each(function (index, el) {
            input_names.push(el);
        });

        var aux = valida_inputs(input_names, "alert");
        return aux;
    });

    $("#nomeEvento").change(function (event) {
        valida_inputs($("#nomeEvento"));
    });

    $("#publicoAlvo").change(function (event) {
        valida_inputs($("#publicoAlvo"));
    });

    /*-- Validando o campo executor: este só poderá conter letras e espaços, e apóstrofes se necessário --*/
    $("#executor").change(function (event) {
        str = $("#executor").val();
        regexp = new RegExp(/^[A-Za-záàâãéèêíïóôõöúçñÁÀÂÃÉÈÍÏÓÔÕÖÚÇÑ'\s]+$/);
        if ((!regexp.test(str))) {
            $("#executor").val("");
            $("#executor").focus();
        }
        valida_inputs($("#executor"), "alert");
    });

    $("#localEvento").change(function (event) {
        valida_inputs($("#localEvento"));
    });

    $("#tipo").change(function (event) {
        valida_inputs($("#tipo"), "alert");
    });

    $("#periodo").change(function (event) {
        valida_inputs($("#periodo"), "alert");
    });

    $("#quantVagas").change(function (event) {
        str = $("#quantVagas").val();
        regexp = new RegExp(/^[0-9]/);
        if ((!regexp.test(str))) {
            $("#quantVagas").val("");
            $("#quantVagas").focus();
        }
        valida_inputs($("#quantVagas"), "alert");
    });

    $("#descricao").change(function (event) {
        quantChar = $("#descricao").val().length;
        if (quantChar < 40) {
            $("#alert").text("Campo descrição deve possuir no mínimo 40 caracteres").css({"text-align": "center", "font-weight": "bold"});
            $("#alert").css({"display": "block"});
            $("#descricao").css({"border": "2px solid red"});
            $("#descricao").focus();
        } else {
            $("#descricao").css({"border": "2px solid green"});
        }
    });

    $("#requisitos").change(function (event) {
        valida_inputs($("#requisitos"), "alert");
    });

    $("#cancelar").click(function () {
        $(location).attr('href', 'admin_page.html');
    })
});

/*------------------------------Segunda Parte------------------------------*/

function clearForm() {
    $("#nomeEvento").val("");
    $("#localEvento").val("");
    $("#publicoAlvo").val("");
    $("#tipo").val("");
    $("#executor").val("");
    $("#quantVagas").val("");
    ($("#dataEvento").val(""));
    $("#horaEvento").val("");
    $("#descricao").val("");
    $("#requisitos").val("");
    $("#foto").val("");
}

$("#cadastro_evento").submit(function (event) {

    event.preventDefault();

    var nomeDoEvento        = $("#nomeEvento").val();
    var localDoEvento       = $("#localEvento").val();
    var publicoAlvo         = $("#publicoAlvo").val();
    var tipo                = $("#tipo").val();
    var executor            = $("#executor").val();
    var quantidadeDeVagas   = $("#quantVagas").val();
    var dataDoEvento        = new Date($("#dataEvento").val());
    var horaDoEvento        = $("#horaEvento").val();
    var descEvento          = $("#descricao").val();
    var requisitosEvento    = $("#requisitos").val();
    var imagemEvento        = "evento.png"; // ajuste
    
    //Desculpa Cris <3 era necessário
    horaDoEvento = "1970-01-01T" + horaDoEvento + ":00" + "-03:00";

    var novo_evento = {"dataEvento": dataDoEvento, "descricao": descEvento, "horaEvento": horaDoEvento, "imagem": imagemEvento,
        "local": localDoEvento, "nomeEvento": nomeDoEvento, "publicoAlvo": publicoAlvo, "quantidadeVagas": quantidadeDeVagas,
        "requisitos": requisitosEvento, "responsaveis": executor, "status": 1, "tipo": tipo
    };

    dataDoEvento.setDate(dataDoEvento.getDate()+1); // o objeto traz o dia decrementado por algum motivo

    $.ajax({
        url: "http://localhost:8080/Trabalho_de_Web_II_-_Sistema_da_Semana_da_Engenharia_UNIFAE_-_Java/webresources/services.eventos/cadastro_evento",
        dataType: 'json',
        type: 'POST',
        data: JSON.stringify(novo_evento),
        contentType: 'application/json;charset=utf-8',
        success: function (data) {
            if (data) {
                clearForm();
                $("#success").text("Evento cadastrado com sucesso!").css({"text-align": "center", "font-weight": "bold"});
                $("#success").css({"display": "block"});
            } else {
                $("#alert").text("Já existe um evento cadastrado neste dia, hora e local").css({"text-align": "center", "font-weight": "bold"});
                $("#alert").css({"display": "block"});
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            $("#alert").text("Ocorreu um erro ao cadastrar novo evento! " + thrownError).css({"text-align": "center", "font-weight": "bold"});
            $("#alert").css({"display": "block"});
        }
    });
});