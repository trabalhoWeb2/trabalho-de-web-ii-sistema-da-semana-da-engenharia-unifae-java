var input_names;

/*-- Função que controla o aparecimento da caixa de alerta ao usuario: esta deve receber um elemento, 
o seu estado (se esta correto o preenchimento do campo ou não), 
e um alert que indica qual caixa deverá mudar de estado (alert ou modal_alert) --*/
function alert_personalizado(campo, estado, alert){
	if (!estado) {
		$("#"+alert).text("Preencha o(s) campo(s) " + campo + " corretamente").css({"text-align":"center", "font-weight":"bold"});
		$("#"+alert).css({"display": "block"});
	} else {
		$("#"+alert).css({"display": "none"});
	}
}

/*-- Função que garante que não sejam submetidos inputs com valor em branco e captura 
seus nomes para alerta --*/
function valida_inputs(input_names, alert) {
	var id;
	var string = "";
	var aux = true;

	$(input_names).each(function(index, el) {
		id = el.id;
		if ($("#"+id).val() == "") {
			$("#"+id).css({"border": "2px solid red"});
			string += " " + el.name;
			aux = false;
		} else {
			$("#"+id).css({"border": "2px solid green"});
		}
	});

	alert_personalizado(string, aux, alert);

	return aux;
}

jQuery(document).ready(function($) {

	/*-- Validando as informações de Cadastro para não haverem campos vazios --*/
	$("#alterar").click(function() {
		input_names = new Array();

		$("#quantVagas").each(function(index, el) {
			input_names.push(el);
        });
        
		$(".form-control-file").each(function(index, el) {
			input_names.push(el);
		})
		
		var aux = valida_inputs(input_names, "alert");
		return aux;      
	});

    $("#executor").change(function(event) {		
		str = $("#executor").val();
		regexp = new RegExp(/^[A-Za-záàâãéèêíïóôõöúçñÁÀÂÃÉÈÍÏÓÔÕÖÚÇÑ'\s]+$/);
		if((!regexp.test(str))) {
			$("#executor").val("");
			$("#executor").focus();
		}
		valida_inputs($("#executor"), "alert");
    });
    
    $("#descricao").change(function(event) {
		quantChar = $("#descricao").val().length;
		if(quantChar<40){
			$("#alert").text("Campo descrição deve possuir no mínimo 40 caracteres").css({"text-align":"center", "font-weight":"bold"});
			$("#alert").css({"display": "block"});
            $("#descricao").css({"border": "2px solid red"});
            $("#descricao").val("");
			$("#descricao").focus();
		}else{
			$("#descricao").css({"border": "2px solid green"});
		}
	});

	$("#requisitos").change(function(event) {
		valida_inputs($("#requisitos"), "alert");
	});

	$("#quantVagas").change(function(event) {		
		str = $("#quantVagas").val();
		regexp = new RegExp(/^[0-9]/);
		if((!regexp.test(str))) {
			$("#quantVagas").val("");
			$("#quantVagas").focus();
		}
		valida_inputs($("#quantVagas"), "alert");
	});

	$("#cancelar").click(function() {
		$(location).attr('href', 'admin_page.html');
	})

    /********************************************* Segunda Parte ***********************************************/

    // captura a url e tira o id do evento mandado por ela
    url_atual = window.location.href;
    array = url_atual.split("?");
    array = array[1].split("=");
    id_evento = array[1];
    descricao;

    $.ajax({
        method: "GET",
        dataType: "json",
        url: "http://localhost:8080/Trabalho_de_Web_II_-_Sistema_da_Semana_da_Engenharia_UNIFAE_-_Java/webresources/services.eventos/" + id_evento,
        success: function (data) {
            // formatando a data
            dataDoEvento = new Date(data['dataEvento']);
            stringData = dataDoEvento.getDate() + "/" + (dataDoEvento.getMonth() + 1) + "/" + dataDoEvento.getFullYear();
            // formatando a hora
            horaEvento = new Date(data['horaEvento']);
            stringHora = horaEvento.getHours() + ":" + horaEvento.getMinutes();

            imagem = data['imagem'];

            $("#nomeEvento").val(data['nomeEvento']);
            $("#localEvento").val(data['local']);
            $("#publicoAlvo").val(data['publicoAlvo']);
            $("#tipo").val(data['tipo']);
            $("#executor").val(data['responsaveis']);
            $("#quantVagas").val(data['quantidadeVagas']);
            $("#dataEvento").val(stringData);
            $("#horaEvento").val(stringHora);
            $("#descricao").val(data['descricao']);
            $("#requisitos").val(data['requisitos']);

            descricao = data['descricao'];
        }
    });

});

$("#editar_evento").submit(function (event) {
    
    event.preventDefault();

    var nomeDoEvento        = $("#nomeEvento").val();
    var localDoEvento       = $("#localEvento").val();
    var publicoAlvo         = $("#publicoAlvo").val();
    var tipo                = $("#tipo").val();
    var executor            = $("#executor").val();
    var quantidadeDeVagas   = $("#quantVagas").val();
    var horaDoEvento        = $("#horaEvento").val();
    var descEvento          = $("#descricao").val();
    var requisitosEvento    = $("#requisitos").val();
    var imagemEvento        = "evento.png";

    //Desculpa Cris <3 era necessário
    horaDoEvento = "1970-01-01T" + horaDoEvento + ":00" + "-03:00";

    var evento_edit = {
        "dataEvento": dataDoEvento, "descricao": descEvento, "horaEvento": horaDoEvento, "ideventos": id_evento, "imagem": imagemEvento,
        "local": localDoEvento, "nomeEvento": nomeDoEvento, "publicoAlvo": publicoAlvo, "quantidadeVagas": quantidadeDeVagas,
        "requisitos": requisitosEvento, "responsaveis": executor, "status": 1, "tipo": tipo
    };

    $.ajax({
        url: "http://localhost:8080/Trabalho_de_Web_II_-_Sistema_da_Semana_da_Engenharia_UNIFAE_-_Java/webresources/services.eventos/" + id_evento,
        dataType: 'json',
        type: 'PUT',
        data: JSON.stringify(evento_edit),
        contentType: 'application/json;charset=utf-8',
        success: function (data) {
            $("#success").text("Evento atualizado com sucesso!").css({ "text-align": "center", "font-weight": "bold" });
            $("#success").css({ "display": "block" });
            window.location.reload;
        },
        error: function (xhr, ajaxOptions, thrownError) {
            $("#alert").text("Ocorreu um erro ao atualizar o evento! " + thrownError).css({ "text-align": "center", "font-weight": "bold" });
            $("#alert").css({ "display": "block" });
        }
    });

});

$("#cancelar").click(function () {
    $(location).attr('href', 'admin_page.html');
})