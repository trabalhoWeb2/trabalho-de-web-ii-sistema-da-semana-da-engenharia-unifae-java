function atualiza_inscricao(id_usuario, id_evento) {
    var aux = true;
    var numero_de_inscricoes = 0;

    $.ajax({
        method: "GET",
        dataType: "json",
        url: "http://localhost:8080/Trabalho_de_Web_II_-_Sistema_da_Semana_da_Engenharia_UNIFAE_-_Java/webresources/services.inscricoes/atualiza_inscricao/"+id_usuario+"/"+id_evento,
        success: function (data) {
            var items = [];
            for (var index = 0; index < data.length; index++) {
                items.push("<li>" + data[index]['nome'] + "</li>");   
            }
            $("#lista_inscritos" + id_evento).html(items);
            $("#qtdInscritos" + id_evento).html(items.length);
            $("#modal_qtdInscritos" + id_evento).html(items.length);
            if ($("#modalButton" + id_evento).hasClass("fa-check")) {
                numero_de_inscricoes = +$("#numero_de_inscricoes").text();
                numero_de_inscricoes--;
                $("#numero_de_inscricoes").html(numero_de_inscricoes);
                alert("Desinscrição feita com sucesso");
            }
            if ($("#modalButton" + id_evento).hasClass("fa-check-square")) {
                numero_de_inscricoes = +$("#numero_de_inscricoes").text();
                numero_de_inscricoes++;
                $("#numero_de_inscricoes").html(numero_de_inscricoes);
                alert("Inscrição feita com sucesso");
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert(xhr);
            alert(thrownError);
        },
    });
}

function muda_status(id) {
    $.ajax({
        method: "GET",
        url: "http://localhost:8080/Trabalho_de_Web_II_-_Sistema_da_Semana_da_Engenharia_UNIFAE_-_Java/webresources/services.eventos/update_status?id_evento="+id,
        success: function (data) {
            alert(data);
        }
    });
}

function numero_de_inscricoes(id_evento){
    $.ajax({
        url: "http://localhost:8080/Trabalho_de_Web_II_-_Sistema_da_Semana_da_Engenharia_UNIFAE_-_Java/webresources/services.inscricoes/numero_inscritos;id_evento=" + id_evento,
        dataType: 'json',
        type: 'GET',
        success: function (count) {
            $("#qtdInscritos"+id_evento).html(count);
            $("#qtdInscritos_modal"+id_evento).html(count);
        }
    });
}

function incricoes_usuario(){
    $.ajax({
        url: "http://localhost:8080/Trabalho_de_Web_II_-_Sistema_da_Semana_da_Engenharia_UNIFAE_-_Java/webresources/services.inscricoes/incricoes_usuario?id_usuario=" + localStorage.getItem('id_usuario'),
        type: 'GET',
        success: function (count) {
            $("#numero_de_inscricoes").html(count);
        }
    });
}

function verifica_inscricao(id_evento){
    $.ajax({
        url: "http://localhost:8080/Trabalho_de_Web_II_-_Sistema_da_Semana_da_Engenharia_UNIFAE_-_Java/webresources/services.inscricoes/verifica_inscricao?id_usuario=" + localStorage.getItem('id_usuario') + "&id_evento=" + id_evento,
        type: 'GET',
        success: function (data) {
            $("#btn_inscrever-se"+id_evento).append('<i class="fa fa-check-square toggleButton" id="modalButton' + id_evento + '"></i>');
            $("#btn_inscrever-se_modal_"+id_evento).append('<i class="fa fa-check-square toggleButton" id="modalButton' + id_evento + '"></i><span class="inscreva-se">Desinscrever-se</span>');
        },
        error: function (){
            $("#btn_inscrever-se"+id_evento).append('<i class="fa fa-check toggleButton" id="modalButton' + id_evento + '"></i>');
            $("#btn_inscrever-se_modal_"+id_evento).append('<i class="fa fa-check toggleButton" id="modalButton' + id_evento + '"></i><span class="inscreva-se">Inscrever-se</span>');
        }
    });
}

function atualiza_lista_incritos(id_usuario, id_evento){
    $.ajax({
        url: "http://localhost:8080/Trabalho_de_Web_II_-_Sistema_da_Semana_da_Engenharia_UNIFAE_-_Java/webresources/services.inscricoes/nomes_usuarios_inscritos/"+id_usuario+"/"+id_evento,
        type: 'GET',
        dataType: "json",
        success: function (data) {
            var items = [];
            for (var index = 0; index < data.length; index++) {
                items.push("<li>" + data[index]['nome'] + "</li>"); 
            }
            $("#lista_inscritos" + id_evento).html(items);
        }
    });
}

function visualizar_eventos(id_usuario, id_evento) {
    $.ajax({
      method: "GET",
      dataType: "json",
      url: "http://localhost:8080/Trabalho_de_Web_II_-_Sistema_da_Semana_da_Engenharia_UNIFAE_-_Java/webresources/services.inscricoes/nomes_usuarios_inscritos/"+id_usuario+"/"+id_evento,
      success: function (data) {
        var items = [];
        for (var index = 0; index < data.length; index++) {
            items.push("<li>" + data[index]['nome'] + "</li>");
        }
        $("#lista_inscritos" + id_evento).html(items);
        }
    });
}

function gerar_lista_eventos(id_usuario, id_evento) {
    $.ajax({
      method: "GET",
      dataType: "json",
      url: "http://localhost:8080/Trabalho_de_Web_II_-_Sistema_da_Semana_da_Engenharia_UNIFAE_-_Java/webresources/services.inscricoes/nomes_usuarios_inscritos/"+id_usuario+"/"+id_evento,
      success: function (data) {
        var items = [];
        for (var index = 0; index < data.length; index++) {
            items.push(data);
        }
        return(items);
        }
    });
}

function numero_de_inscricoes(id_evento){
    $.ajax({
        url: "http://localhost:8080/Trabalho_de_Web_II_-_Sistema_da_Semana_da_Engenharia_UNIFAE_-_Java/webresources/services.inscricoes/numero_inscritos;id_evento=" + id_evento,
        dataType: 'json',
        type: 'GET',
        success: function (count) {
            $("#qtdInscritos"+id_evento).html(count);
            $("#qtdInscritos_modal"+id_evento).html(count);
        }
    });
}

function incricoes_usuario(){
    $.ajax({
        url: "http://localhost:8080/Trabalho_de_Web_II_-_Sistema_da_Semana_da_Engenharia_UNIFAE_-_Java/webresources/services.inscricoes/incricoes_usuario?id_usuario=" + localStorage.getItem('id_usuario'),
        type: 'GET',
        success: function (count) {
            $("#numero_de_inscricoes").html(count);
        }
    });
}