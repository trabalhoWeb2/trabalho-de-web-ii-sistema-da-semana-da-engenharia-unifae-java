var num_inscritos = 0

// desativa eventos desabilitados por administradores do sistema
function desativar_evento(id) {
    $("#" + id).addClass("desativado");
    $("#modal" + id).addClass("desativado");
    if ($("#modalButton" + id).hasClass("fa-toggle-off")) { //Detecta se é um botão de ativar ou desativar evento
        $(".toggleButton").filter("#modalButton" + id).removeClass("fa-toggle-off");
        $(".toggleButton").filter("#modalButton" + id).addClass("fa-toggle-on");
    } else {
        $(".toggleButton").filter("#modalButton" + id).removeClass("fa-toggle-on");
        $(".toggleButton").filter("#modalButton" + id).addClass("fa-toggle-off");
    }

}

/*Função para mudar css ao desativar o evento*/
function addClassByClick(id) {
    if ($("#" + id).hasClass("desativado")) {
        /*Mudar estado*/
        $("#" + id).removeClass("desativado");
        $("#modal" + id).removeClass("desativado");
        /*Mudar botão*/
        $(".toggleButton").filter("#modalButton" + id).removeClass("fa-toggle-off");
        $(".toggleButton").filter("#modalButton" + id).addClass("fa-toggle-on");
        /*Mudar tooltip*/
        $("#tooltipText" + id).tooltip("dispose");
        $("#tooltipText" + id).attr({ "title": "Desativar evento" }).tooltip();
        /*Mudar texto modal*/
        $("#modalText" + id).val("");
        $("#modalText" + id).text(" Desativar evento");

    } else {
        $("#" + id).addClass("desativado");
        $("#modal" + id).addClass("desativado");

        $(".toggleButton").filter("#modalButton" + id).removeClass("fa-toggle-on");
        $(".toggleButton").filter("#modalButton" + id).addClass("fa-toggle-off");

        $("#tooltipText" + id).tooltip("dispose");
        $("#tooltipText" + id).attr({ "title": "Ativar evento" }).tooltip();


        $("#modalText" + id).val("");
        $("#modalText" + id).text(" Ativar evento");
    }
}

function redirect (id_evento){
    window.location.href="admin_editar_evento.html?id_evento="+id_evento;
}

function lotacao(id_evento) {
    $.ajax({
        url: "http://localhost:8080/Trabalho_de_Web_II_-_Sistema_da_Semana_da_Engenharia_UNIFAE_-_Java/webresources/services.inscricoes/numero_inscritos;id_evento=" + id_evento,
        type: 'GET',
        success: function (count) {
            qtdDisponivel = +$("#qtdDisponivel" + (id_evento)).text();
            if (count == qtdDisponivel) {
                num_inscritos++;
                $("#qtdEventosLotados").html(num_inscritos);
            }
        }
    });
}