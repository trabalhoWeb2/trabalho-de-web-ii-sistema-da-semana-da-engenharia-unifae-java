function block_ao_logar(id_usuario) {
	$.ajax({
		method: "get",
		dataType: "json",
		url: "http://localhost:8080/Trabalho_de_Web_II_-_Sistema_da_Semana_da_Engenharia_UNIFAE_-_Java/webresources/services.inscricoes/dados_eventos_inscritos?id_usuario=" + id_usuario,
		success: function (data) {
			for (var i = 0; i < data.length; i++) {
				blockInscricaoEvento(data[i]['inscricoesPK']['eventoId']);
			}
		},
	});
}

// desativa eventos desabilitados por administradores do sistema
function desativar_evento(id) {
	$("#" + id).addClass("desativado");
	$("#modal" + id).addClass("desativado");
	$("#btn_inscrever-se" + (id)).prop("disabled", true);
	$("#btn_inscrever-se_modal_" + (id)).prop("disabled", true);
	$("#btn_inscrever-se" + (id)).tooltip("dispose");
	$("#btn_inscrever-se" + (id)).attr({ "title": "Evento desativado, todas as inscrições foram canceladas, pedimos desculpas pelo transtorno!" }).tooltip();
}

// verifica a data atual do sistema, e consulta na base de dados a data do evento atual, caso sejam iguais, o evento é bloqueado
function diaDeEventoBloquear(id_evento) {
	var dataAtual = new Date();
	var stringData = dataAtual.getFullYear() + "-" + (dataAtual.getMonth() + 1) + "-" + dataAtual.getDate(); //pega a data atual
	$.ajax({
		method: "get",
		dataType: "json",
		url: "http://localhost:8080/Trabalho_de_Web_II_-_Sistema_da_Semana_da_Engenharia_UNIFAE_-_Java/webresources/services.eventos/" + id_evento,
		success: function (data) {
			var dataEvento = new Date(data['dataEvento']);
			var stringDataEvento = dataEvento.getFullYear() + "-" + (dataEvento.getMonth() + 1) + "-" + dataEvento.getDate(); //pega a data atual
			if (!$("#" + (id_evento)).hasClass("desativado")) {
				if (stringData == stringDataEvento) {
					//Desativa o botão
					$("#btn_inscrever-se" + id_evento).prop("disabled", true);
					$("#btn_inscrever-se_modal_" + id_evento).prop("disabled", true);
					$("#btn_inscrever-se" + id_evento).tooltip("dispose");
					$("#btn_inscrever-se" + id_evento).attr({ "title": "O evento é hoje! Inscrições e desinscrições bloqueadas!" }).tooltip();
				} else if (dataAtual.getDate() > (dataEvento.getDate() + 1) || (dataAtual.getMonth() + 1) > (dataEvento.getMonth() + 1) || dataAtual.getFullYear() > dataEvento.getFullYear()) { // a condição não é das melhores
					$("#btn_inscrever-se" + id_evento).prop("disabled", true);
					$("#btn_inscrever-se_modal_" + id_evento).prop("disabled", true);
					$("#btn_inscrever-se" + id_evento).tooltip("dispose");
					$("#btn_inscrever-se" + id_evento).attr({ "title": "O evento já ocorreu!" }).tooltip();
				}
			}
		},
	});
}

// bloqueia eventos que estão com as vagas esgotadas -- menos para usuarios ja inscritos neste
function lotacao(id_evento) {
	var aux; // caso o evento não seja bloqueado por lotação, o sistema irá verificar a data 
	$.ajax({
		method: "get",
		url: "http://localhost:8080/Trabalho_de_Web_II_-_Sistema_da_Semana_da_Engenharia_UNIFAE_-_Java/webresources/services.eventos/lotacao/" + id_evento,
		success: function (data) {
			/* quando um usuario se inscreve na ultima vaga disponivel, o evento não será bloqueado para este usuário, porém para outros sim.
			Mas por algum motivo isso não acontece, mas se a instrução abaixo for descomentada, funciona! */
			if (data == 1) {
				if (!$("#" + (id_evento)).hasClass("desativado")) {
					if ($("#modalButton" + id_evento).hasClass("fa-check")) { //Detecta se é um botão de inscrição
						//Desativa o botão
						$("#btn_inscrever-se" + id_evento).prop("disabled", true);
						$("#btn_inscrever-se_modal_" + id_evento).prop("disabled", true);
						$("#btn_inscrever-se" + id_evento).tooltip("dispose");
						$("#btn_inscrever-se" + id_evento).attr({ "title": "Evento Lotado!" }).tooltip();
					}
				}
			}
		}
	});
}

// desbloqueia o evento de mesma data quando o usuário se desinscrever deste
function unblockEvento(id_evento) {
	var data_evento;
	$.ajax({
		method: "get",
		dataType: "json",
		url: "http://localhost:8080/Trabalho_de_Web_II_-_Sistema_da_Semana_da_Engenharia_UNIFAE_-_Java/webresources/services.eventos",
		success: function (data) {
			var dados_eventos = [];
			for (var index = 0; index < data.length; index++) {
				dados_eventos[index] = [];
				dados_eventos[index]['id'] = data[index]['ideventos'];
				dados_eventos[index]['dataEvento'] = data[index]['dataEvento'];
				dados_eventos[index]['horaEvento'] = data[index]['horaEvento'];
			}
			for (var index = 0; index < dados_eventos.length; index++) {
				if (dados_eventos[index]['id'] == id_evento) {
					data_evento = dados_eventos[index]['dataEvento'];
				}
			}
			for (var index = 0; index < dados_eventos.length; index++) {
				if (dados_eventos[index]['dataEvento'] == data_evento) {
					if (!$("#" + dados_eventos[index]['id']).hasClass("desativado")) {
						if ($("#modalButton" + dados_eventos[index]['id']).hasClass("fa-check")) { //Detecta se é um botão de inscrição
							//Ativa o botão
							$("#btn_inscrever-se" + dados_eventos[index]['id']).prop("disabled", false);
							$("#btn_inscrever-se_modal_" + dados_eventos[index]['id']).prop("disabled", false);
							$("#btn_inscrever-se" + dados_eventos[index]['id']).tooltip("dispose");
							$("#btn_inscrever-se" + dados_eventos[index]['id']).attr({ "title": "Inscrever-se" }).tooltip();
							lotacao(dados_eventos[index]['id']); // só pra garantir
							diaDeEventoBloquear(dados_eventos[index]['id']);
						}
					}
				}
			}
		}
	});
}

// função que bloqueia um evento de mesma data que o evento que o usuario se inscreveu
function blockInscricaoEvento(id_evento) {
	var data_evento;
	$.ajax({
		method: "get",
		dataType: "json",
		url: "http://localhost:8080/Trabalho_de_Web_II_-_Sistema_da_Semana_da_Engenharia_UNIFAE_-_Java/webresources/services.eventos",
		success: function (data) {
			var dados_eventos = [];
			// traz os dados relevantes para bloqueio de outros eventos que tenham a mesma data que o evento que o usuario se inscreveu
			for (var index = 0; index < data.length; index++) {
				dados_eventos[index] = [];
				dados_eventos[index]['id'] = data[index]['ideventos'];
				dados_eventos[index]['dataEvento'] = data[index]['dataEvento'];
				dados_eventos[index]['horaEvento'] = data[index]['horaEvento'];
			}
			// todas as datas de eventos são trazidas acima, aqui definimos qual é a data do evento que o usuario se inscreveu
			for (var index = 0; index < dados_eventos.length; index++) {
				if (dados_eventos[index]['id'] == id_evento) {
					data_evento = dados_eventos[index]['dataEvento'];
				}
			}
			//tendo a data do evento em mãos, percorremos todas as datas de eventos, e bloqueamos aqueles que possuem a mesma data
			for (var index = 0; index < dados_eventos.length; index++) {
				if (dados_eventos[index]['dataEvento'] == data_evento) {
					if (!$("#" + dados_eventos[index]['id']).hasClass("desativado")) { // um evento desativado não deve sofrer essa verificação
						if ($("#modalButton" + dados_eventos[index]['id']).hasClass("fa-check")) {//Detecta se é um botão de inscrição
							//Desativa o botão
							$("#btn_inscrever-se" + dados_eventos[index]['id']).prop("disabled", true);
							$("#btn_inscrever-se_modal_" + dados_eventos[index]['id']).prop("disabled", true);
							$("#btn_inscrever-se" + dados_eventos[index]['id']).tooltip("dispose");
							$("#btn_inscrever-se" + dados_eventos[index]['id']).attr({ "title": "Você já se cadastrou em um evento neste periodo!" }).tooltip();
							lotacao(dados_eventos[index]['id']);
							diaDeEventoBloquear(dados_eventos[index]['id']);
						}
					}
				}
			}
		}
	});
}

// troca os icones de inscricão e desinscrição
function addClassByClick(id) {
	if ($("#modalButton" + id).hasClass("fa-check")) {//Caso não esteja inscrito no evento
		$("#modalButton" + id).removeClass("fa-check");
		$(".toggleButton").filter("#modalButton" + id).removeClass("fa-check");
		$("#btn_inscrever-se" + id).children(".inscreva-se").val("");
		$("#btn_inscrever-se_modal_" + id).children(".inscreva-se").val("");
		$("#btn_inscrever-se" + id).children(".inscreva-se").text(" Desinscrever-se");
		$("#btn_inscrever-se_modal_" + id).children(".inscreva-se").text(" Desinscrever-se");
		$("#btn_inscrever-se" + id).tooltip("dispose");
		$("#btn_inscrever-se" + id).attr({ "title": "Desinscrever-se" }).tooltip();
		$(".toggleButton").filter("#modalButton" + id).addClass("fa-check-square");
		blockInscricaoEvento(id);
	} else {
		$("#modalButton" + id).addClass("fa-check");
		$(".toggleButton").filter("#modalButton" + id).removeClass("fa-check-square");
		$("#btn_inscrever-se" + id).children(".inscreva-se").val("");
		$("#btn_inscrever-se_modal_" + id).children(".inscreva-se").val("");
		$("#btn_inscrever-se" + id).children(".inscreva-se").text(" Inscrever-se");
		$("#btn_inscrever-se_modal_" + id).children(".inscreva-se").text(" Inscrever-se");
		$("#btn_inscrever-se" + id).tooltip("dispose");
		$("#btn_inscrever-se" + id).attr({ "title": "Inscrever-se" }).tooltip();
		$(".toggleButton").filter("#modalButton" + id).addClass("fa-check");
		unblockEvento(id);
	}
}

// ações realizadas ao se inscrever ou se desinscrever de um evento
function acoes(id_evento, id_usuario) {
	addClassByClick(id_evento);
	atualiza_inscricao(id_usuario, id_evento);
}